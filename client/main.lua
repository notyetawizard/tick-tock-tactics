local network = require "libs/network"
local game = require "libs/game"

function love.load()
    --some graphics things
    love.graphics.setDefaultFilter("nearest")
    love.graphics.setLineStyle("rough")
    
    win_w, win_h = love.window.getMode()
    tile_s = 32
    
    network:client()
    network:send("create_token")
end

controls = {
    dx = 0,
    dy = 0,
    up = "w",
    left = "a",
    down = "s",
    right = "d"
}

function love.keypressed(key, scan)
    if scan == controls.up then controls.dy = controls.dy - 1
    elseif scan == controls.left then controls.dx = controls.dx - 1
    elseif scan == controls.down then controls.dy = controls.dy + 1
    elseif scan == controls.right then controls.dx = controls.dx + 1
    end
end


function love.keyreleased(key, scan)
    if scan == controls.up then controls.dy = controls.dy + 1
    elseif scan == controls.left then controls.dx = controls.dx + 1
    elseif scan == controls.down then controls.dy = controls.dy - 1
    elseif scan == controls.right then controls.dx = controls.dx - 1
    end
end


function love.update(dt)
    if game.tokens[network.token] then
        game.tokens[network.token].tx = game.tokens[network.token].x + controls.dx
        game.tokens[network.token].ty = game.tokens[network.token].y + controls.dy
        if game.tokens[network.token].mv_dt == 0
        and (game.tokens[network.token].tx ~= game.tokens[network.token].x
        or game.tokens[network.token].ty ~= game.tokens[network.token].y) then
            network:send("tx="..game.tokens[network.token].tx.." ty="..game.tokens[network.token].ty)
        end
    end
    
    dg = network:receive()
    if dg then
        print(dg)
        if string.match(dg, "^[%x%-]+: .+") then
            tuid, info = string.match(dg, "^([%x%-]+): (.+)")
            if not game.tokens[tuid] then game.tokens[tuid] = {} end
            for key, value in string.gmatch(info, "([^ ]+)=([^ ]+)") do
                if tonumber(value) then
                    game.tokens[tuid][key] = tonumber(value)
                elseif value == "true" then
                    game.tokens[tuid][key] = true
                elseif value == "false" then
                    game.tokens[tuid][key] = false
                else game.tokens[tuid][key] = value
                end
            end
        elseif string.match(dg, "^%d+ %d+ .+") then
            x, y, info = string.match(dg, "^(%d+) (%d+) (.+)")
            x, y = tonumber(x), tonumber(y)
            if not game.board[y] then game.board[y] = {} end
            if not game.board[y][x] then game.board[y][x] = {} end
            for key, value in string.gmatch(info, "([^ ]+)=([^ ]+)") do
                if tonumber(value) then
                    game.board[y][x][key] = tonumber(value)
                elseif value == "true" then
                    game.board[y][x][key] = true
                elseif value == "false" then
                    game.board[y][x][key] = false
                else game.board[y][x][key] = value
                end
            end
        end
    end

    for tuid, token in pairs(game.tokens) do
        game:setMoveClient(tuid)
        game:moveClient(dt, tuid)
    end
end

function love.draw()
    love.graphics.scale(1)
    love.graphics.setLineWidth(1)
    
    for y = 0, win_h, tile_s do
        love.graphics.line(0, y, win_w, y)
    end
    for x = 0, win_w, tile_s do
        love.graphics.line(x, 0, x, win_h)
    end
    
    if game.tokens[network.token] then
        love.graphics.print({{255,255,255,255}, "ap = "..game.tokens[network.token].ap.."\nx = "..game.tokens[network.token].tx.."\ny = "..game.tokens[network.token].ty}, 10, 10)
    end

    for tuid, token in pairs(game.tokens) do
        love.graphics.rectangle("fill",
            (token.x + token.dx*token.mv_dt/token.mv_spd) * tile_s,
            (token.y + token.dy*token.mv_dt/token.mv_spd) * tile_s,
            tile_s, tile_s)
    end
end
