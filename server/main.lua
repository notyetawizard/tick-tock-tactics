local network = require "libs/network"
local game = require "libs/game"

function love.load()
    network:server()
    game:newToken(network.spare_uuid)
end

function love.update(dt)
    tuid, info = network:receive()
    if tuid then
        if info == "create_token" then
            if not game.tokens[tuid] then
                game:newToken(tuid)
            else game:updateToken(tuid)
            end
        else
            local updates = {}
            for key, value in string.gmatch(info, "([^ ]+)=([^ ]+)") do
                if game.tokens[tuid][key] ~= value
                and game.tokens[tuid][key] ~= tonumber(value) then
                    if tonumber(value) then
                        updates[key] = tonumber(value)
                    else updates[key] = value
                    end
                end
            end
            game:updateToken(tuid, updates)
        end
    end
    
    if network.new_client == true then
        for tuid, token in pairs(game.tokens) do
            local dg = tuid..":"
            for key, value in pairs(token) do
                dg = dg.." "..key.."="..value
            end
            network:send(dg)
        end
        for ky, vy in pairs(game.board) do
            for kx, vx in pairs(vy) do
                dg = kx.." "..ky
                for key, value in pairs (vx) do
                    dg = dg.." "..key.."="..tostring(value)
                end
                network:send(dg)
            end
        end
        network.new_client = nil
    end
    
    for tuid, token in pairs(game.tokens) do
        game:setMoveServer(tuid)
        game:moveServer(dt, tuid)
    end
    
    for tuid, updates in pairs(game.updates.tokens) do
        if next(updates) then
            local dg = tuid..":"
            for key, value in pairs(updates) do
                dg = dg.." "..key.."="..tostring(value)
            end
            game.updates.tokens[tuid] = nil
            network:send(dg)
        end
    end
    for ky, vy in pairs(game.updates.board) do
        for kx, vx in pairs(vy) do
            dg = kx.." "..ky
            for key, value in pairs (vx) do
                dg = dg.." "..key.."="..tostring(value)
            end
            game.updates.board[kx] = nil
            network:send(dg)
        end
        game.updates.board[ky] = nil
    end
end
