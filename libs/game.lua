local game = {
    board = {},
    tokens = {},
    updates = {
        board = {},
        tokens = {}
    }
}

function game:isSpaceEmpty(x, y)
    --eventually, the missing spaces should return false, as they will be outside the defined game board; for now, no board is defined so spaces are infinite, and must be created on the fly
    if not self.board[y] then self.board[y] = {} end
    if not self.board[y][x] then self.board[y][x] = {} end
    if not self.board[y][x].token and not self.board[y][x].full then
        return true end
    return false
end

--updateToken and updateBoard send their changes to the clients in addition to makeing changes internally
function game:updateToken(tuid, updates)
    if not self.updates.tokens[tuid] then self.updates.tokens[tuid] = {} end
    if updates == nil then
        for key, value in pairs(self.updates.tokens[tuid]) do
            self.updates.tokens[tuid][key] = value
        end
    else
        for key, value in pairs(updates) do
            self.tokens[tuid][key] = value
            self.updates.tokens[tuid][key] = value
        end
    end
end

function game:updateBoard(x, y, updates)
    if not self.updates.board[y] then self.updates.board[y] = {} end
    if not self.updates.board[y][x] then self.updates.board[y][x] = {} end
    if updates == nil then
        for key, value in pairs(self.updates.board[y][x]) do
            self.updates.board[y][x][key] = value
        end
    else
        for key, value in pairs(updates) do
            self.board[y][x][key] = value
            self.updates.board[y][x][key] = value
        end
    end
end

function game:newToken(tuid)
    while true do
        x, y = math.random(1,18), math.random(1,20)
        if self:isSpaceEmpty(x, y) then break end
    end
    game.tokens[tuid] = {}
    self:updateToken(tuid, {
        x = x,
        y = y,
        tx = x,
        ty = y,
        dx = 0,
        dy = 0,
        ap = 6,
        ap_max = 6,
        ap_dt = 0,
        mv_spd = 0.5,
        mv_dt = 0
    })
    self:updateBoard(x, y, {token = tuid, full = true})
end

function game:setMoveServer(tuid)
    local token = self.tokens[tuid]
    if (token.tx ~= token.x or token.ty ~= token.y) and token.mv_dt == 0 then
        if self:isSpaceEmpty(token.tx, token.ty) then
            self:updateBoard(token.tx, token.ty, {full = true})
            self:updateToken(tuid, {tx = token.tx, ty = token.ty})
            token.dx = token.tx - token.x
            token.dy = token.ty - token.y
        else self:updateToken(tuid, {tx = token.x, ty = token.y})
            token.mv_dt = -token.mv_spd
        end
    end
end

function game:moveServer(dt, tuid)
    local token = self.tokens[tuid]
    if token.dx ~= 0 and token.dy ~= 0 then
        if token.mv_dt < 0 then
            token.mv_dt = token.mv_dt + dt/1.42
            if token.mv_dt >= 0 then
                token.dx, token.dy, token.mv_dt = 0, 0, 0
            end                
        else token.mv_dt = token.mv_dt + dt/1.42
        end
    elseif token.dx ~= 0 or token.dy ~= 0 then
        if token.mv_dt < 0 then
            token.mv_dt = token.mv_dt + dt
            if token.mv_dt >= 0 then
                token.dx, token.dy, token.mv_dt = 0, 0, 0
            end
        else token.mv_dt = token.mv_dt + dt
        end
    elseif token.mv_dt < 0 then
        token.mv_dt = token.mv_dt + dt
        if token.mv_dt >= 0 then
            token.mv_dt = 0
        end
    end
    
    if token.mv_dt >= token.mv_spd/2 then
        self:updateBoard(token.x, token.y, {token = false, full = false})
        token.x = token.x + token.dx
        token.y = token.y + token.dy
        self:updateBoard(token.x, token.y, {token = tuid, full = true})
        token.mv_dt = token.mv_dt - token.mv_spd
    end
end

function game:setMoveClient(tuid)
    local token = self.tokens[tuid]
    if (token.tx ~= token.x or token.ty ~= token.y) and token.mv_dt == 0 then
        if self:isSpaceEmpty(token.tx, token.ty) then
            token.dx = token.tx - token.x
            token.dy = token.ty - token.y
        else token.mv_dt = -token.mv_spd
        end
    end
end

function game:moveClient(dt, tuid)
    local token = self.tokens[tuid]
    if token.dx ~= 0 and token.dy ~= 0 then
        if token.mv_dt < 0 then
            token.mv_dt = token.mv_dt + dt/1.42
            if token.mv_dt >= 0 then
                token.dx, token.dy, token.mv_dt = 0, 0, 0
            end                
        else token.mv_dt = token.mv_dt + dt/1.42
        end
    elseif token.dx ~= 0 or token.dy ~= 0 then
        if token.mv_dt < 0 then
            token.mv_dt = token.mv_dt + dt
            if token.mv_dt >= 0 then
                token.dx, token.dy, token.mv_dt = 0, 0, 0
            end
        else token.mv_dt = token.mv_dt + dt
        end
    elseif token.mv_dt < 0 then
        token.mv_dt = token.mv_dt + dt
        if token.mv_dt >= 0 then
            token.mv_dt = 0
        end
    end
    
    if token.mv_dt >= token.mv_spd/2 then
        token.x = token.x + token.dx
        token.y = token.y + token.dy
        token.mv_dt = token.mv_dt - token.mv_spd
    end
end

return game
