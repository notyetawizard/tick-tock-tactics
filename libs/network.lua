local socket = require "socket"

local uuid = {
    used = {}
}

function uuid:new()
    local uuidgen = io.popen("uuidgen")
    new_id = uuidgen:read("*l")
    uuidgen:close()
    for _, used_id in pairs(self.used) do
        if used_id == new_id then self:new() end
    end
    table.insert(self.used, new_id)
    return new_id
end

local network = {}
network.address = "localhost"
network.port = 9995
--network.user = "tester1"
--network.user = "tester2"
network.spare_uuid = uuid:new()

function network:client()
    --start the network, as a client or server
    --this could probably be cleaner, perhaps seperated into two distinct functions.
    self.udp = socket.udp()
    self.udp:settimeout(0)
    self.udp:setpeername(self.address, self.port)
    
    function self:send(dg)
        self.udp:send(self.id.." "..self.token..": "..dg)
    end
    
    function self:receive()
        local dg = self.udp:receive()
        if dg then
            --network messages
            if string.match(dg, "^network .+") then
                request, info = string.match(dg, "^([%w_]+) (.+)", 9)
                if string.match(request, "connected") then
                    self.id, self.token = string.match(info, "^([%x%-]+) ([%x%-]+)")
                end
            --game messages just get passed through
            else return dg
            end
        end
    end
            
    while not self.id or not self.token do
        self.udp:send("network connect "..self.user)
        self:receive()
        socket.sleep(0.5)
    end
    
end

function network:server()
    --implement waiting for (all?) clients to connect
    self.udp = socket.udp()
    self.udp:settimeout(0)
    self.udp:setsockname("*", self.port)
    self.clients = {}
    
    function self:send(dg)   
        for cuid, client in pairs(self.clients) do
            self.udp:sendto(dg, client.ip, client.port)
        end
    end
    
    function self:receive()
        local dg, ip, port = self.udp:receivefrom()
        if dg then
            print(dg)
            --network messages
            if string.match(dg, "^network .+") then
                request, info = string.match(dg, "^([%w_]+) (.+)", 9)
                --connect with a new client, or reconnect with a disconnected client
                if request == "connect" then
                    local function assignClient(username)
                        for cuid, client in pairs(self.clients) do
                            if client.username == username then
                                return cuid, client.token
                            end
                        end 
                        return uuid:new(), uuid:new()
                    end
                    local cuid, tuid = assignClient(info)
                    self.new_client = true
                    self.clients[cuid] = {ip = ip, port = port, username = info, token = tuid}
                    self.udp:sendto("network connected "..cuid.." "..tuid, ip, port)
                end
            --game messages
            elseif string.match(dg, "^[%x%-]+ [%x%-]+: .+") then
                cuid, tuid, info = string.match(dg, "^([%x%-]+) ([%x%-]+): (.+)")
                if self.clients[cuid] and self.clients[cuid].token == tuid then
                    return tuid, info
                end
            end
        end
    end
end

return network
